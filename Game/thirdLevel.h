void runThirdLevel(RenderWindow &window)
{
	for (int i = 0; i < HEIGHT_MAP; i++)
	{
		mountainMap[i] = MOUNTAINMAP[i];
	}
	level = 3;
	mus soundTrack("soundtrack.wav");
	soundTrack.play();
	std::list<entity*>  entities;
	std::list<entity*>::iterator it;
	for (int i = 0; i < MOUNTAINENEMIES; i++)
	{
		entities.push_back(new enemy("mountainEnemy.png", mountainEnemiesCoordsX[i], mountainEnemiesCoordsY[i] - 10, ENEMYWIDTH, ENEMYHEIGHT));
	}
	environment map("mountainMap.png");
	environment coin("mountainCoin.png");
	environment spike("mountainSpike.png");
	environment key("keys.png");
	Character hero("superhero.png", 240, GROUND - 10, HEROWIDTH, HEROHEIGHT);
	Character oldMan("oldMan.png", 11700, 2430, OLDMANWIDTH, OLDMANHEIGHT);
	Character girl("girl.png", 2300, 570, GIRLWIDTH, GIRLHEIGHT);
	Font font;
	font.loadFromFile("fonts/arial.ttf");
	Text text("", font, 20);
	text.setFont(font);
	text.setColor(Color::Black);
	text.setCharacterSize(20);
	Text questTimeText("", font, 40);
	questTimeText.setFont(font);
	questTimeText.setColor(Color::Red);
	questTimeText.setCharacterSize(25);
	view.reset(FloatRect(0, 0, WIDTH, HEIGHT));
	Clock clock;
	Clock gameTimeClock;
	Clock questTimeClock;
	float time = 0, gameTime = 0, questTime = QUESTTIME;
	while (window.isOpen())
	{
		gameTime = gameTimeClock.getElapsedTime().asSeconds();
		time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		time = time / 500;
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
		}
		hero.setDirection();
		if (isGameBroken == true)
		{
			window.close();
			return;
		}
		if (hero.health<=0)
		{
			showDeathScreen();
			lives--;
			hero.scores = 0;
			soundTrack.pause();
			runThirdLevel(window);
		}
		if (lives == 0)
		{
			lives = 1;
			window.close();
			return;
		}
		if (hero.health <= 0)
		for (it = entities.begin(); it != entities.end(); it++)
		{
			(*it)->update(time);
		}
		hero.update(time);
		if (abs(hero.x - oldMan.x) <= 100)
		{
			if ((Keyboard::isKeyPressed(Keyboard::Return) && hero.mission == false) || (Keyboard::isKeyPressed(Keyboard::Return) && hero.mission == true && hero.findGirl == true))
			{
				hero.setDialog(level);
				clock.restart();
				questTimeClock.restart();
				hero.sprite.setPosition(hero.x, hero.y);

			}
		}
		if (abs(hero.x - girl.x) <= 100)
		{
			if (Keyboard::isKeyPressed(Keyboard::Return) && hero.mission == true && hero.findGirl == false)
			{
				hero.setDialog(level);
				clock.restart();
				hero.sprite.setPosition(hero.x, hero.y);
			}
		}
		if (hero.findGirl == true)
		{
			girl.sprite.setPosition(11550, 2430);
		}
		if (Keyboard::isKeyPressed(Keyboard::Escape))
		{
			runGameMenu();
			clock.restart();
		}
		for (it = entities.begin(); it != entities.end(); it++)
		{
			if ((*it)->getRect().intersects(hero.getRect()))
			{
				if (hero.dy > 0 && hero.onGround == false)
				{
					murderEnemy.play();
					hero.dy = -0.3;
					(*it)->health = 0;
					(*it)->dx = 0;
					hero.scores += 100;
				}
				else
				{
					if (hero.undeadTime == 0)
					{
						hero.health -= 10;
						hero.sprite.setColor(Color::Red);
					}
					hero.undead = true;
					hero.update(time);
					break;
				}
			}
		}
		for (it = entities.begin(); it != entities.end(); it++)
		{
			entity *b = *it;
			b->update(time);
			if ((*it)->health == 0)
			{
				it = entities.erase(it); delete b;
			}
		}
		viewMap(time);
		window.setView(view);
		window.clear();
		for (int i = 0; i < HEIGHT_MAP; i++)
		{
			for (int j = 0; j < WIDTH_MAP; j++)
			{
				if (mountainMap[i][j] == '0')  map.sprite.setTextureRect(IntRect(0, 0, TILEWIDTH, TILEHEIGHT));
				if (mountainMap[i][j] == ' ' || mountainMap[i][j] == 'i')  map.sprite.setTextureRect(IntRect(TILEWIDTH * 7, 0, TILEWIDTH, TILEHEIGHT));
				if (mountainMap[i][j] == '1')  map.sprite.setTextureRect(IntRect(TILEWIDTH, 0, TILEWIDTH, TILEHEIGHT));
				if (mountainMap[i][j] == '4') map.sprite.setTextureRect(IntRect(TILEWIDTH * 4, 0, TILEWIDTH, TILEHEIGHT));
				if (mountainMap[i][j] == '5') map.sprite.setTextureRect(IntRect(TILEWIDTH * 5, 0, TILEWIDTH, TILEHEIGHT));
				if (mountainMap[i][j] == '6') map.sprite.setTextureRect(IntRect(TILEWIDTH * 6, 0, TILEWIDTH, TILEHEIGHT));
				if (mountainMap[i][j] == 'c')
				{
					coin.sprite.setTextureRect(IntRect(0, 0, TILEWIDTH, TILEHEIGHT));
					coin.sprite.setPosition(j * TILEWIDTH, i * TILEHEIGHT);
					window.draw(coin.sprite);
					continue;
				}
				if ((mountainMap[i][j] == 's'))
				{
					spike.sprite.setTextureRect(IntRect(0, 0, TILEWIDTH, TILEHEIGHT));
					spike.sprite.setPosition(j * TILEWIDTH, i * TILEHEIGHT);
					window.draw(spike.sprite);
					continue;
				}
				if ((hero.mission == false))
				{
					mountainMap[39][189] = ' ';
				}
				else
				{
					mountainMap[39][189] = '1';
				}
				map.sprite.setPosition(j * TILEWIDTH, i * TILEHEIGHT);
				window.draw(map.sprite);
			}
		}
		for (int i = 0; i < keys; i++)
		{
			key.sprite.setTextureRect(IntRect(i * KEYWIDTH, 0, KEYWIDTH, KEYHEIGHT));
			key.sprite.setPosition(view.getCenter().x - WIDTH / 2 + 10 + 30 * i, view.getCenter().y - HEIGHT / 2 + 10);
			window.draw(key.sprite);
		}
		for (it = entities.begin(); it != entities.end(); it++)
		{
			window.draw((*it)->sprite);
		}
		window.draw(girl.sprite);
		window.draw(oldMan.sprite);
		window.draw(hero.sprite);
		if (hero.mission == true && hero.findGirl == false)
		{
			ostringstream questTimeString;
			questTime = QUESTTIME - questTimeClock.getElapsedTime().asSeconds()+1;
			if (questTime == 0)
			{
				hero.health = 0;
			}
			questTimeString << static_cast <int>(questTime);
			questTimeText.setColor(Color::Red);
			questTimeText.setString("REMAINING TIME: " + questTimeString.str() + '\n');
			questTimeText.setPosition(view.getCenter().x - WIDTH / 2 + 10, view.getCenter().y - HEIGHT / 2 + 150);
			window.draw(questTimeText);
		}
		else if (hero.mission==false)
		{
			questTime = 0;
			questTimeClock.restart();
		}
		ostringstream scoresString;
		ostringstream livesString;
		ostringstream gameTimeString;
		ostringstream healthString;
		scoresString << hero.scores;
		livesString << lives;
		healthString << hero.health;
		gameTimeString << static_cast <int>(gameTime);
		text.setString("SCORES: " + scoresString.str() + '\n' + "TIME: " + gameTimeString.str() + '\n' + "HEALTH: " + healthString.str() + '%' + '\n' + "LIVES: " + livesString.str());
		text.setPosition(view.getCenter().x - WIDTH / 2 + 10, view.getCenter().y - HEIGHT / 2 + 10);
		text.setPosition(view.getCenter().x - WIDTH / 2 + 10, view.getCenter().y - HEIGHT / 2 + 50);
		window.draw(text);
		window.display();
	}
}
