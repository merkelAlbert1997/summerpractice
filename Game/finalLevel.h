void runFinalLevel(RenderWindow &window)
{
	for (int i = 0; i < HEIGHT_CAVEMAP; i++)
	{
		caveMap[i] = CAVEMAP[i];
	}
	level = 4;
	mus soundTrack("soundtrack.wav");
	soundTrack.play();
	environment map("caveMap.png");
	environment key("keys.png");
	Character hero("superhero.png", 240, CAVEGROUND - 10, HEROWIDTH, HEROHEIGHT);
	Character treeChest("treeChestClosed.png", 500, CAVEGROUND-TREECHESTHEIGHT, TREECHESTWIDTH, TREECHESTHEIGHT);
	Character ironChest("ironChestClosed.png", 700, CAVEGROUND - IRONCHESTHEIGHT, IRONCHESTWIDTH, IRONCHESTHEIGHT);
	Character silverChest("silverChestClosed.png", 900, CAVEGROUND - SILVERCHESTHEIGHT, SILVERCHESTWIDTH, SILVERCHESTHEIGHT);
	Character goldenChest("goldenChestClosed.png", 1100, CAVEGROUND - GOLDENCHESTHEIGHT, GOLDENCHESTWIDTH, GOLDENCHESTHEIGHT);
	Font font;
	font.loadFromFile("fonts/arial.ttf");
	Text text("", font, 20);
	text.setFont(font);
	text.setColor(Color::Black);
	text.setCharacterSize(20);
	view.reset(FloatRect(0, 0, WIDTH, HEIGHT));
	Clock clock;
	Clock gameTimeClock;
	float time = 0, gameTime = 0;
	while (window.isOpen())
	{
		gameTime = gameTimeClock.getElapsedTime().asSeconds();
		time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		time = time / 500;
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
		}
		hero.setDirection();
		if (isGameBroken == true)
		{
			window.close();
			return;
		}
		if (Keyboard::isKeyPressed(Keyboard::Escape))
		{
			runGameMenu();
			clock.restart();
		}
		if (abs(hero.x - treeChest.x)<=100)
		{
			if (keys == 0)
			{
				if (Keyboard::isKeyPressed(Keyboard::Return))
				{
					treeChest.texture.loadFromFile("images/treeChestOpened.png");
				}
			}
		}
		if (abs(hero.x - ironChest.x)<=100)
		{
			if (keys == 1)
			{
				if (Keyboard::isKeyPressed(Keyboard::Return))
				{
					ironChest.texture.loadFromFile("images/ironChestOpened.png");
				}
			}
		}
		if (abs(hero.x - silverChest.x)<=100)
		{
			if (keys == 2)
			{
				if (Keyboard::isKeyPressed(Keyboard::Return))
				{
					silverChest.texture.loadFromFile("images/silverChestOpened.png");
				}
			}
		}
		if (abs(hero.x - goldenChest.x)<=100)
		{
			if (keys == 3)
			{
				if (Keyboard::isKeyPressed(Keyboard::Return))
				{
					goldenChest.texture.loadFromFile("images/goldenChestOpened.png");
				}
			}
		}
		hero.update(time);
		viewMap(time);
		window.setView(view);
		window.clear();
		for (int i = 0; i < HEIGHT_CAVEMAP; i++)
		{
			for (int j = 0; j < WIDTH_CAVEMAP; j++)
			{
				if (caveMap[i][j] == ' ')  map.sprite.setTextureRect(IntRect(TILEWIDTH, 0, TILEWIDTH, TILEHEIGHT));
				if (caveMap[i][j] == '1')  map.sprite.setTextureRect(IntRect(0, 0, TILEWIDTH, TILEHEIGHT));
				if ((caveMap[i][j] == 'g')) map.sprite.setTextureRect(IntRect(TILEWIDTH *2, 0, TILEWIDTH, TILEHEIGHT));
				map.sprite.setPosition(j * TILEWIDTH, i * TILEHEIGHT);
				window.draw(map.sprite);
			}
		}
		for (int i = 0; i < keys; i++)
		{
			key.sprite.setTextureRect(IntRect(i * KEYWIDTH, 0, KEYWIDTH, KEYHEIGHT));
			key.sprite.setPosition(view.getCenter().x - WIDTH / 2 + 10 + 30 * i, view.getCenter().y - HEIGHT / 2 + 10);
			window.draw(key.sprite);
		}
		window.draw(treeChest.sprite);
		window.draw(ironChest.sprite);
		window.draw(silverChest.sprite);
		window.draw(goldenChest.sprite);
		window.draw(hero.sprite);
		ostringstream scoresString;
		ostringstream livesString;
		ostringstream gameTimeString;
		ostringstream healthString;
		scoresString << hero.scores;
		livesString << lives;
		healthString << hero.health;
		gameTimeString << static_cast <int>(gameTime);
		text.setString("SCORES: " + scoresString.str() + '\n' + "TIME: " + gameTimeString.str() + '\n' + "HEALTH: " + healthString.str() + '%' + '\n' + "LIVES: " + livesString.str());
		text.setPosition(view.getCenter().x - WIDTH / 2 + 10, view.getCenter().y - HEIGHT / 2 + 10);
		text.setPosition(view.getCenter().x - WIDTH / 2 + 10, view.getCenter().y - HEIGHT / 2 + 50);
		window.draw(text);
		window.display();
	}
}
