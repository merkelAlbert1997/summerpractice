void runSecondLevel(RenderWindow &window)
{
	for (int i = 0; i < HEIGHT_MAP; i++)
	{
		castleMap[i] = CASTLEMAP[i];
	}
	level = 2;
	mus soundTrack("soundtrack.wav");
	soundTrack.play();
	std::list<entity*>  entities;
	std::list<entity*>::iterator it;
	for (int i = 0; i < CASTLEENEMIES; i++)
	{
		entities.push_back(new enemy("castleEnemy.png", castleEnemiesCoordsX[i], castleEnemiesCoordsY[i] - 10, ENEMYWIDTH, ENEMYHEIGHT));
	}
	environment map("map_castle.png");
	environment coin("castleCoin.png");
	environment spike("castleSpike.png");
	environment key("keys.png");
	element castleDoor;
	setPropertiesOfElement(castleDoor, 11940, 2310, 60, 90);
	setSpriteOfElement(castleDoor, "castleDoor.png");
	setPositionOfElement(castleDoor);
	Character hero("superhero.png", 240, GROUND - 10, HEROWIDTH, HEROHEIGHT);
	Character king("king.png", 11607, 2310, KINGWIDTH, KINGHEIGHT);
	Character rat("rat.png", 8782, 1530, RATWIDTH, RATHEIGHT);
	Font font;
	font.loadFromFile("fonts/arial.ttf");
	Text text("", font, 20);
	text.setFont(font);
	text.setColor(Color::Black);
	text.setCharacterSize(20);
	view.reset(FloatRect(0, 0, WIDTH, HEIGHT));
	Clock clock;
	Clock gameTimeClock;
	float time = 0, gameTime = 0;
	while (window.isOpen())
	{
		gameTime = gameTimeClock.getElapsedTime().asSeconds();
		time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		time = time / 500;
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
		}
		hero.setDirection();
		if (isGameBroken == true)
		{
			window.close();
			return;
		}
		if (hero.health <= 0)
		{
			showDeathScreen();
			lives--;
			hero.scores = 0;
			soundTrack.pause();
			runSecondLevel(window);
		}
		if (lives == 0)
		{
			lives = 1;
			window.close();
			return;
		}
		if (hero.x >= 11930 && hero.y == 2310)
		{
			soundTrack.pause();
			runThirdLevel(window);
		}
		for (it = entities.begin(); it != entities.end(); it++)
		{
			(*it)->update(time);
		}
		hero.update(time);
		if (abs(hero.x - king.x) <= 100)
		{
			if ((Keyboard::isKeyPressed(Keyboard::Return) && hero.mission == false) || (Keyboard::isKeyPressed(Keyboard::Return) && hero.mission == true && hero.winRat == true))
			{
				hero.setDialog(level);
				clock.restart();
				hero.sprite.setPosition(hero.x, hero.y);
			}
		}
		if (abs(hero.x - rat.x) <= 200)
		{
			if ((Keyboard::isKeyPressed(Keyboard::Return) && hero.mission == true && hero.winRat == false && hero.isGamingWithRat == false))
			{
				hero.setDialog(level);
				clock.restart();
				hero.sprite.setPosition(hero.x, hero.y);
			}
			if (hero.mission == true && hero.winRat == false && hero.isGamingWithRat == true)
			{
				hero.setHeroRatGame();
				clock.restart();
				hero.sprite.setPosition(hero.x, hero.y);
			}
		}
		if (Keyboard::isKeyPressed(Keyboard::Escape))
		{
			runGameMenu();
			clock.restart();
		}
		for (it = entities.begin(); it != entities.end(); it++)
		{
			if ((*it)->getRect().intersects(hero.getRect()))
			{
				if (hero.dy > 0 && hero.onGround == false)
				{
					murderEnemy.play();
					hero.dy = -0.3;
					(*it)->health = 0;
					(*it)->dx = 0;
					hero.scores += 100;
				}
				else
				{
					if (hero.undeadTime == 0)
					{
						hero.health -= 10;
						hero.sprite.setColor(Color::Red);
					}
					hero.undead = true;
					hero.update(time);
					break;
				}
			}
		}
		for (it = entities.begin(); it != entities.end(); it++)
		{
			entity *b = *it;
			b->update(time);
			if ((*it)->health == 0)
			{
				it = entities.erase(it); delete b;
			}
		}
		viewMap(time);
		window.setView(view);
		window.clear();
		for (int i = 0; i < HEIGHT_MAP; i++)
		{
			for (int j = 0; j < WIDTH_MAP; j++)
			{
				if (castleMap[i][j] == ' ' || castleMap[i][j] == 'i')  map.sprite.setTextureRect(IntRect(TILEWIDTH, 0, TILEWIDTH, TILEHEIGHT));
				if (castleMap[i][j] == '1')  map.sprite.setTextureRect(IntRect(0, 0, TILEWIDTH, TILEHEIGHT));
				if ((castleMap[i][j] == 'f')) map.sprite.setTextureRect(IntRect(TILEWIDTH * 2, 0, TILEWIDTH, TILEHEIGHT));
				if ((castleMap[i][j] == 'c'))
				{
					coin.sprite.setTextureRect(IntRect(0, 0, TILEWIDTH, TILEHEIGHT));
					coin.sprite.setPosition(j * TILEWIDTH, i * TILEHEIGHT);
					window.draw(coin.sprite);
					continue;
				}
				if ((castleMap[i][j] == 's'))
				{
					spike.sprite.setTextureRect(IntRect(0, 0, TILEWIDTH, TILEHEIGHT));
					spike.sprite.setPosition(j * TILEWIDTH, i * TILEHEIGHT);
					window.draw(spike.sprite);
					continue;
				}
				if ((hero.mission == false))
				{
					castleMap[37][183] = ' ';
				}
				else
				{
					castleMap[37][183] = '1';
				}
				map.sprite.setPosition(j * TILEWIDTH, i * TILEHEIGHT);
				window.draw(map.sprite);
			}
		}
		for (int i = 0; i < keys; i++)
		{
			key.sprite.setTextureRect(IntRect(i * KEYWIDTH, 0, KEYWIDTH, KEYHEIGHT));
			key.sprite.setPosition(view.getCenter().x - WIDTH / 2 + 10 + 30 * i, view.getCenter().y - HEIGHT / 2 + 10);
			window.draw(key.sprite);
		}
		for (it = entities.begin(); it != entities.end(); it++)
		{
			window.draw((*it)->sprite);
		}
		if ((hero.winRat == false) || (abs(hero.x - rat.x) <= WIDTH))
		{
			window.draw(rat.sprite);
		}
		window.draw(castleDoor.sprite);
		window.draw(king.sprite);
		window.draw(hero.sprite);
		ostringstream scoresString;
		ostringstream livesString;
		ostringstream gameTimeString;
		ostringstream healthString;
		scoresString << hero.scores;
		livesString << lives;
		healthString << hero.health;
		gameTimeString << static_cast <int>(gameTime);
		text.setString("SCORES: " + scoresString.str() + '\n' + "TIME: " + gameTimeString.str() + '\n' + "HEALTH: " + healthString.str() + '%' + '\n' + "LIVES: " + livesString.str());
		text.setPosition(view.getCenter().x - WIDTH / 2 + 10, view.getCenter().y - HEIGHT / 2 + 10);
		text.setPosition(view.getCenter().x - WIDTH / 2 + 10, view.getCenter().y - HEIGHT / 2 + 50);
		window.draw(text);
		window.display();
	}
}
