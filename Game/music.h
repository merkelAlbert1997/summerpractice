#include <SFML/Graphics.hpp>
using namespace sf;
class mus
{
private:
	sf::Music music;
public:
	mus(String file);
	void play();
	void pause();
};

mus::mus(String file)
{
	music.openFromFile("sounds/" + file);
}

inline void mus::play()
{
	music.play();
	music.setLoop(true);

}
inline void mus::pause()
{
	music.pause();
}