#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <sstream>
#include <list>
#include "Constants.h"
#include "menu.h"
#include "mainMenu.h"
#include "gameMenu.h"
#include "death.h"
#include "dialogs.h"
#include "Sounds.h"
#include "music.h"
#include "map.h"
#include "castleMap.h"
#include "mountainMap.h"
#include "caveMap.h"
#include "view.h"
#include "Entity.h"
#include "Character.h"
#include "enemies.h"
#include "Environment.h"
#include "finalLevel.h"
#include "thirdLevel.h"
#include "secondLevel.h"
#include "firstLevel.h"
using namespace sf;
using namespace std;

int main()
{
	isGaming = true;
	while (isGaming == true)
	{
		runMainMenu();
		if (isGameRun == true)
		{
			RenderWindow window(VideoMode(WIDTH, HEIGHT), "Game", Style::Fullscreen);
			runFinalLevel(window);
		}
	}
	return 0;
}
