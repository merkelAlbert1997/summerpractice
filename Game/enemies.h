#include <SFML/Graphics.hpp>
using namespace sf;
int forestEnemiesCoordsX[FORESTENEMIES] = { 3739,4300,4856,6206,6691,7247,9439,9014,8599,8185,4654 };
int forestEnemiesCoordsY[FORESTENEMIES] = { 2460,2460,2460,2280,2280,2280,960,960,960,960,960 };
int castleEnemiesCoordsX[CASTLEENEMIES] = { 2273,2685,4823,4832,5348,5348,5838,5838,5173,5649,8053,8588,9007, 9273 };
int castleEnemiesCoordsY[CASTLEENEMIES] = { 2280,2280,2520,2820,2520,2820,2820,2520,2040,2040,2340,2340,2340,2340 };
int mountainEnemiesCoordsX[MOUNTAINENEMIES] = {2915,3775,2908,3762,4543,4543};
int mountainEnemiesCoordsY[MOUNTAINENEMIES] = {2580,2580,2820,2820,2580,2820};
class enemy :public  entity
{
public:
	bool onGround = false;
	enemy(String _f, float _x, float _y, float _w, float _h);
	void update(float _time);
	void collision();
};
enemy::enemy(String _f, float _x, float _y, float _w, float _h)
{
	file = _f;
	x = _x;
	y = _y;
	w = _w;
	h = _h;
	//onGround = true;
	Image hero_image;
	hero_image.loadFromFile("images/" + file);
	texture.loadFromImage(hero_image);
	sprite.setTexture(texture);
	sprite.setTextureRect(IntRect(0, 0, w, h));
	sprite.setPosition(x, y);
}

void enemy::update(float _time)
{
	collision();
	x += dx*_time;
	if (!onGround)
	{
		dy += 0.001*_time;
	}
	y += dy;
	if (dx > 0)
	{
		sprite.setTextureRect(IntRect(0, 0, ENEMYWIDTH, ENEMYHEIGHT));
	}
	if (dx < 0)
	{
		sprite.setTextureRect(IntRect(ENEMYWIDTH, 0, ENEMYWIDTH, ENEMYHEIGHT));
	}
	sprite.setPosition(x, y);
}

void enemy::collision()
{
	String *map = 0;
	if (level == 1) map = forestMap;
	if (level == 2) map = castleMap;
	if (level == 3) map = mountainMap;
	for (int i = y / TILEHEIGHT; i < (y + ENEMYHEIGHT) / TILEHEIGHT; i++)
	{
		for (int j = x / TILEWIDTH; j < (x + ENEMYWIDTH) / TILEWIDTH; j++)
		{
			if ((map[i][j] != '0' && map[i][j] != ' ' && map[i][j] != 'c' && map[i][j] != 's'&& map[i][j] != 'f') || (i < 1) || (j < 1))
			{
				if (dy > 0)//������������ ��� �������� ����
				{
					y = i * TILEHEIGHT - ENEMYHEIGHT;
					dy = 0;
					onGround = true;
					dx = DX;
				}
			}
			if (map[i][j] == 's')
			{
				if (dx > 0)
				{
					x = j * TILEWIDTH - ENEMYWIDTH;
					dx = -DX / 2;
					continue;
				}
				if (dx < 0)
				{
					x = j * TILEWIDTH + TILEWIDTH;
					dx = DX / 2;
					continue;
				}
			}
		}
	}
};
