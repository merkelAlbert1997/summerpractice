#include <SFML/Graphics.hpp>
using namespace sf;
int treesCoordsX[TREES] = { 305,725,4025,4424,5029,6145,6625,7188,9853,10128,10349,10434,10861,783,1566 };
int treesCoordsY[TREES] = { 2700,2700,2700,2700,2700,2160,2160,2160,2100,2100 ,2100 ,2100 ,2100,480,480 };
class environment
{
public:
	float x = 0, y = 0, w = 0, h = 0;
	String file;
	Texture texture;
	Sprite sprite;
	environment(String _f);
	FloatRect getRect()
	{
		return FloatRect(x, y, w, h);
	}
};
environment::environment(String _f)
{
	file = _f;
	Image obj_image;
	obj_image.loadFromFile("images/" + file);
	texture.loadFromImage(obj_image);
	sprite.setTexture(texture);
};
struct  element
{
	float x = 0, y = 0, w = 0, h = 0;
	String file;
	Texture texture;
	Sprite sprite;
};
void setPropertiesOfElement(element &elem, float _x, float _y, float _w, float _h)
{
	elem.x = _x;
	elem.y = _y;
	elem.w = _w;
	elem.h = _h;
};
void setSpriteOfElement(element &elem, String _f)
{
	elem.file = _f;
	Image obj_image;
	obj_image.loadFromFile("images/" + elem.file);
	elem.texture.loadFromImage(obj_image);
	elem.sprite.setTexture(elem.texture);
	elem.sprite.setTextureRect(IntRect(0, 0, elem.w, elem.h));
};
void setPositionOfElement(element &elem)
{
	elem.sprite.setPosition(elem.x, elem.y);
};
