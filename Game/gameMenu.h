class gameMenuButton :public menuButton
{
public:
	gameMenuButton(String _f, int _x, int _y, int _w, int _h);
};
gameMenuButton::gameMenuButton(String _f, int _x, int _y, int _w, int _h)
{
	x = _x;
	y = _y;
	w = _w;
	h = _h;
	texture.loadFromFile("images/" + _f);
	sprite.setTexture(texture);
	sprite.setTextureRect(IntRect(0, 0, _w, _h));
	sprite.setPosition(_x, _y);
}
void runGameMenu()
{
	isGameBroken = false;
	RenderWindow menuWindow(VideoMode(WIDTH, HEIGHT), "MENU", Style::None);
	gameMenuButton goOn("continue.png", WIDTH / 2 - 75, HEIGHT / 2, 150, 50);
	gameMenuButton exit("exit.png", WIDTH / 2 - 75, HEIGHT / 2 + 50, 150, 50);
	Texture backGround;
	backGround.loadFromFile("images/backGroundMenu.png");
	Sprite backGroundSprite;
	backGroundSprite.setTexture(backGround);
	backGroundSprite.setTextureRect(IntRect(0, 0, WIDTH, HEIGHT));
	backGroundSprite.setPosition(0, 0);
	while (menuWindow.isOpen())
	{
		menuWindow.clear(Color(223, 158, 0));
		goOn.sprite.setColor(Color::White);
		exit.sprite.setColor(Color::White);
		Event event;
		while (menuWindow.pollEvent(event))
		{
			if (goOn.getRectangle().contains(Mouse::getPosition(menuWindow)))
			{
				goOn.sprite.setColor(Color::Green);
			}
			if (exit.getRectangle().contains(Mouse::getPosition(menuWindow)))
			{
				exit.sprite.setColor(Color::Green);
			}
			if (Mouse::isButtonPressed(Mouse::Left))
			{
				if (goOn.getRectangle().contains(Mouse::getPosition(menuWindow)))
				{
					menuWindow.close();
				}
				if (exit.getRectangle().contains(Mouse::getPosition(menuWindow)))
				{
					isGameBroken = true;
					menuWindow.close();
					return;
				}
			}
			menuWindow.draw(backGroundSprite);
			menuWindow.draw(goOn.sprite);
			menuWindow.draw(exit.sprite);
			menuWindow.display();
		}
	}
}