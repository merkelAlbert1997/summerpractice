void runFirstLevel(RenderWindow &window)
{
	level = 1;
	for (int i = 0; i < HEIGHT_MAP; i++)
	{
		forestMap[i] = FORESTMAP[i];
	}
	mus soundTrack("soundtrack.wav");
	soundTrack.play();
	std::list<entity*>  entities;
	std::list<entity*>::iterator it;
	for (int i = 0; i < FORESTENEMIES; i++)
	{
		entities.push_back(new enemy("forestEnemy.png", forestEnemiesCoordsX[i], forestEnemiesCoordsY[i] - 10, ENEMYWIDTH, ENEMYHEIGHT));
	}
	environment map("map.png");
	environment coin("coin.png");
	environment spike("spike.png");
	environment ring("ring.png");
	environment key("keys.png");
	Character hero("superhero.png", 240, GROUND - 10, HEROWIDTH, HEROHEIGHT);
	Character richman("Richman.png", 11300, 2190, RICHMANWIDTH, RICHMANHEIGHT);
	element trees[TREES];
	for (int i = 0; i < TREES; i++)
	{
		setPropertiesOfElement(trees[i], treesCoordsX[i], treesCoordsY[i] + 10, 180, 180);
		setSpriteOfElement(trees[i], "tree.png");
		setPositionOfElement(trees[i]);
	}
	element castle;
	setPropertiesOfElement(castle, 11825, 1920, 360, 360);
	setSpriteOfElement(castle, "castle.png");
	setPositionOfElement(castle);
	Font font;
	font.loadFromFile("fonts/arial.ttf");
	Text text("", font, 20);
	text.setFont(font);
	text.setColor(Color::Black);
	text.setCharacterSize(20);
	view.reset(FloatRect(0, 0, WIDTH, HEIGHT));
	Clock clock;
	Clock gameTimeClock;
	float time = 0, gameTime = 0;
	while (window.isOpen())
	{
		gameTime = gameTimeClock.getElapsedTime().asSeconds();
		time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		time = time / 500;
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
		}
		hero.setDirection();
		if (isGameBroken == true)
		{
			window.close();
			return;
		}
		if (hero.health <= 0)
		{
			showDeathScreen();
			lives--;
			hero.scores = 0;
			soundTrack.pause();
			hero.update(time);
			runFirstLevel(window);
		}
		if (lives == 0)
		{
			lives = 1;
			window.close();
			return;
		}
		if (hero.x > 11855 && hero.y == 2190)
		{
			soundTrack.pause();
			runSecondLevel(window);
		}
		for (it = entities.begin(); it != entities.end(); it++)
		{
			(*it)->update(time);
		}
		hero.update(time);
		if (abs(hero.x - richman.x) <= 100)
		{
			if ((Keyboard::isKeyPressed(Keyboard::Return) && hero.mission == false) || (Keyboard::isKeyPressed(Keyboard::Return) && hero.mission == true && hero.withRing == true))
			{
				hero.setDialog(level);
				clock.restart();
				hero.sprite.setPosition(hero.x, hero.y);

			}
		}
		if (Keyboard::isKeyPressed(Keyboard::Escape))
		{
			runGameMenu();
			clock.restart();
		}
		for (it = entities.begin(); it != entities.end(); it++)
		{
			if ((*it)->getRect().intersects(hero.getRect()))
			{
				if (hero.dy > 0 && hero.onGround == false)
				{
					murderEnemy.play();
					hero.dy = -0.3;
					(*it)->health = 0;
					(*it)->dx = 0;
					hero.scores += 100;
				}
				else
				{
					if (hero.undeadTime == 0)
					{
						hero.health -= 10;
						hero.sprite.setColor(Color::Red);
					}
					hero.undead = true;
					hero.update(time);
					break;
				}
			}
		}
		for (it = entities.begin(); it != entities.end(); it++)
		{
			entity *b = *it;
			b->update(time);
			if ((*it)->health == 0)
			{
				it = entities.erase(it); delete b;
			}
		}
		viewMap(time);
		window.setView(view);
		window.clear();
		for (int i = 0; i < HEIGHT_MAP; i++)
		{
			for (int j = 0; j < WIDTH_MAP; j++)
			{
				if (forestMap[i][j] == '0')  map.sprite.setTextureRect(IntRect(0, 0, TILEWIDTH, TILEHEIGHT));
				if (forestMap[i][j] == '1')  map.sprite.setTextureRect(IntRect(TILEWIDTH, 0, TILEWIDTH, TILEHEIGHT));
				if ((forestMap[i][j] == '2')) map.sprite.setTextureRect(IntRect(TILEWIDTH * 2, 0, TILEWIDTH, TILEHEIGHT));
				if ((forestMap[i][j] == '3')) map.sprite.setTextureRect(IntRect(TILEWIDTH * 3, 0, TILEWIDTH, TILEHEIGHT));
				if ((forestMap[i][j] == '4')) map.sprite.setTextureRect(IntRect(TILEWIDTH * 4, 0, TILEWIDTH, TILEHEIGHT));
				if ((forestMap[i][j] == '5')) map.sprite.setTextureRect(IntRect(TILEWIDTH * 5, 0, TILEWIDTH, TILEHEIGHT));
				if ((forestMap[i][j] == '6')) map.sprite.setTextureRect(IntRect(TILEWIDTH * 6, 0, TILEWIDTH, TILEHEIGHT));
				if ((forestMap[i][j] == 'c'))
				{
					coin.sprite.setTextureRect(IntRect(0, 0, TILEWIDTH, TILEHEIGHT));
					coin.sprite.setPosition(j * TILEWIDTH, i * TILEHEIGHT);
					window.draw(coin.sprite);
					continue;
				}
				if ((forestMap[i][j] == 'r'))
				{
					ring.sprite.setTextureRect(IntRect(0, 0, TILEWIDTH, TILEHEIGHT));
					ring.sprite.setPosition(j * TILEWIDTH, i * TILEHEIGHT);
					window.draw(ring.sprite);
					continue;
				}
				if ((forestMap[i][j] == 's'))
				{
					spike.sprite.setTextureRect(IntRect(0, 0, TILEWIDTH, TILEHEIGHT));
					spike.sprite.setPosition(j * TILEWIDTH, i * TILEHEIGHT);
					window.draw(spike.sprite);
					continue;
				}
				if ((hero.mission == false))
				{
					forestMap[35][185] = ' ';
				}
				else
				{
					forestMap[35][185] = '2';
				}
				if (forestMap[i][j] == ' ')  map.sprite.setTextureRect(IntRect(TILEWIDTH * 7, 0, TILEWIDTH, TILEHEIGHT));
				map.sprite.setPosition(j * TILEWIDTH, i * TILEHEIGHT);
				window.draw(map.sprite);
			}
		}
		for (int i = 0; i < keys; i++)
		{
			key.sprite.setTextureRect(IntRect(i * KEYWIDTH, 0, KEYWIDTH, KEYHEIGHT));
			key.sprite.setPosition(view.getCenter().x - WIDTH / 2 + 10 + 30 * i, view.getCenter().y - HEIGHT / 2 + 10);
			window.draw(key.sprite);
		}
		for (int i = 0; i < TREES; i++)
		{
			window.draw(trees[i].sprite);
		}
		window.draw(richman.sprite);
		for (it = entities.begin(); it != entities.end(); it++)
		{
			window.draw((*it)->sprite);
		}
		window.draw(castle.sprite);
		window.draw(hero.sprite);
		ostringstream scoresString;
		ostringstream livesString;
		ostringstream gameTimeString;
		ostringstream healthString;
		scoresString << hero.scores;
		livesString << lives;
		healthString << hero.health;
		gameTimeString << static_cast <int>(gameTime);
		text.setString("SCORES: " + scoresString.str() + '\n' + "TIME: " + gameTimeString.str() + '\n' + "HEALTH: " + healthString.str() + '%' + '\n' + "LIVES: " + livesString.str());
		text.setPosition(view.getCenter().x - WIDTH / 2 + 10, view.getCenter().y - HEIGHT / 2 + 10);
		text.setPosition(view.getCenter().x - WIDTH / 2 + 10, view.getCenter().y - HEIGHT / 2 + 50);
		window.draw(text);
		window.display();
	}
}