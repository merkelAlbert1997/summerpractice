#include <SFML/Graphics.hpp>
Clock undeadClock;
using namespace sf;
class entity {
public:
	float x = 0, y = 0;
	float w = 0, h = 0, dx = 0, dy = 0, currentFrame = 0;
	int dir = 0;
	int health = 100;
	String file;
	Texture texture;
	Sprite sprite;
	FloatRect getRect()
	{
		return FloatRect(x, y, w, h);
	}
	virtual void update(float _time) = 0;
};