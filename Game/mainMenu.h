class mainMenuButton :public menuButton
{
public:
	mainMenuButton(String _f, int _x, int _y, int _w, int _h);
};
mainMenuButton::mainMenuButton(String _f, int _x, int _y, int _w, int _h)
{
		x = _x;
		y = _y;
		w = _w;
		h = _h;
		texture.loadFromFile("images/" + _f);
		sprite.setTexture(texture);
		sprite.setTextureRect(IntRect(0, 0, _w, _h));
		sprite.setPosition(_x, _y);
}
void runMainMenu()
{
	isGameRun = false;
	RenderWindow menuWindow(VideoMode(WIDTH, HEIGHT), "MENU", Style::None);
	mainMenuButton start("start.png", WIDTH / 2 - 75, HEIGHT / 2, 150, 50);
	mainMenuButton controls("controls.png", WIDTH / 2 - 75, HEIGHT / 2+50, 150, 50);
	mainMenuButton authors("authors.png", WIDTH / 2 - 75, HEIGHT / 2+100, 150, 50);
	mainMenuButton exit("exit.png", WIDTH / 2 - 75, HEIGHT / 2+150, 150, 50);
	Texture backGround;
	backGround.loadFromFile("images/backGroundMenu.png");
	Sprite backGroundSprite;
	backGroundSprite.setTexture(backGround);
	backGroundSprite.setTextureRect(IntRect(0, 0, WIDTH, HEIGHT));
	backGroundSprite.setPosition(0, 0);
	while (menuWindow.isOpen())
	{
		menuWindow.clear(Color(223, 158, 0));
		start.sprite.setColor(Color::White);
		controls.sprite.setColor(Color::White);
		authors.sprite.setColor(Color::White);
		exit.sprite.setColor(Color::White);
		Event event;
		while (menuWindow.pollEvent(event))
		{
			if (start.getRectangle().contains(Mouse::getPosition(menuWindow)))
			{
				start.sprite.setColor(Color::Green);
			}
			if (controls.getRectangle().contains(Mouse::getPosition(menuWindow)))
			{
				controls.sprite.setColor(Color::Green);
			}
			if (authors.getRectangle().contains(Mouse::getPosition(menuWindow)))
			{
				authors.sprite.setColor(Color::Green);
			}
			if (exit.getRectangle().contains(Mouse::getPosition(menuWindow)))
			{
				exit.sprite.setColor(Color::Green);
			}
			if (Mouse::isButtonPressed(Mouse::Left))
			{
				if (start.getRectangle().contains(Mouse::getPosition(menuWindow)))
				{
					isGameRun = true;
					isGameBroken = false;
					return;
				}
				if (controls.getRectangle().contains(Mouse::getPosition(menuWindow)))
				{
					menuWindow.close();
					RenderWindow controlsWindow(VideoMode(WIDTH, HEIGHT), "CONTROLS", Style::None);
					mainMenuButton back("back.png", 10, 10, 110, 50);
					Texture controlsMenu;
					controlsMenu.loadFromFile("images/contolsMenu.png");
					Sprite controlsMenuSprite;
					controlsMenuSprite.setTexture(controlsMenu);
					controlsMenuSprite.setTextureRect(IntRect(0, 0, WIDTH, HEIGHT));
					controlsMenuSprite.setPosition(0, 0);
					while (controlsWindow.isOpen())
					{
						controlsWindow.clear(Color(223, 158, 0));
						back.sprite.setColor(Color::White);
						if (back.getRectangle().contains(Mouse::getPosition(controlsWindow)))
						{
							back.sprite.setColor(Color::Green);
						}
						if (Mouse::isButtonPressed(Mouse::Left))
						{
							if (back.getRectangle().contains(Mouse::getPosition(controlsWindow)))
							{
								controlsWindow.close();
								runMainMenu();
							}
						}
						controlsWindow.draw(controlsMenuSprite);
						controlsWindow.draw(back.sprite);
						controlsWindow.display();
					}
				}
				if (authors.getRectangle().contains(Mouse::getPosition(menuWindow)))
				{
					menuWindow.close();
					RenderWindow authorsWindow(VideoMode(WIDTH, HEIGHT), "AUTHORS", Style::None);
					mainMenuButton back("back.png", 10, 10, 110, 50);
					Texture authorsMenu;
					authorsMenu.loadFromFile("images/authorsMenu.png");
					Sprite authorsMenuSprite;
					authorsMenuSprite.setTexture(authorsMenu);
					authorsMenuSprite.setTextureRect(IntRect(0, 0, WIDTH, HEIGHT));
					authorsMenuSprite.setPosition(0, 0);
					while (authorsWindow.isOpen())
					{
						authorsWindow.clear(Color(223, 158, 0));
						back.sprite.setColor(Color::White);
						if (back.getRectangle().contains(Mouse::getPosition(authorsWindow)))
						{
							back.sprite.setColor(Color::Green);
						}
						if (Mouse::isButtonPressed(Mouse::Left))
						{
							if (back.getRectangle().contains(Mouse::getPosition(authorsWindow)))
							{
								authorsWindow.close();
								runMainMenu();
							}
						}
						authorsWindow.draw(authorsMenuSprite);
						authorsWindow.draw(back.sprite);
						authorsWindow.display();
					}
				}
				if (exit.getRectangle().contains(Mouse::getPosition(menuWindow)))
				{
					isGaming = false;
					return;
				}
			}
			menuWindow.draw(backGroundSprite);
			menuWindow.draw(start.sprite);
			menuWindow.draw(controls.sprite);
			menuWindow.draw(authors.sprite);
			menuWindow.draw(exit.sprite);
			menuWindow.display();
		}
	}
}