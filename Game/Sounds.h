#include <SFML/Graphics.hpp>
using namespace sf;
	class tune
	{
	private:
		sf::SoundBuffer buffer;
		sf::Sound sound;
	public:
		tune(String file);
		void play();
	};

	tune::tune(String file)
	{
		buffer.loadFromFile("sounds/" + file);
		sound.setBuffer(buffer);
	}

	inline void tune::play()
	{
		sound.play();
	}