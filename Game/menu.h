#include <SFML/Graphics.hpp>
using namespace sf;
class menuButton
{
public:
	Texture texture;
	Sprite sprite;
	int x = 0, y = 0, w = 0, h = 0;
	IntRect getRectangle()
	{
		return IntRect(x, y, w, h);
	}
};