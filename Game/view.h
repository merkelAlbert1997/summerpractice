#include <SFML/Graphics.hpp>
using namespace sf;
View view;
View getPlayerCoordinateForView(float x, float y)
{
	float tempX = x + HEROWIDTH / 2, tempY = y;
	if (tempX < WIDTH / 2) tempX = WIDTH / 2;
	if (level == 4)
	{
		if (tempX > WIDTH_CAVEMAP*TILEWIDTH - WIDTH / 2) tempX = WIDTH_CAVEMAP*TILEWIDTH - WIDTH / 2;
		if (tempY > CAVEGROUND - HEROHEIGHT - TILEHEIGHT * 2) tempY = CAVEGROUND - HEROHEIGHT - TILEHEIGHT * 2;
	}
	else
	{
		if (tempX > WIDTH_MAP*TILEWIDTH - WIDTH / 2) tempX = WIDTH_MAP*TILEWIDTH - WIDTH / 2;
		if (tempY > GROUND - HEROHEIGHT - TILEHEIGHT * 2) tempY = GROUND - HEROHEIGHT - TILEHEIGHT * 2;
	}
	view.setCenter(tempX, tempY);
	return view;
}
View viewMap(float time)
{
	if (Keyboard::isKeyPressed(Keyboard::Left))
	{
		view.move(-0.1*time, 0);
	}
	if (Keyboard::isKeyPressed(Keyboard::Right))
	{
		view.move(0.1*time, 0);
	}
	return view;
};