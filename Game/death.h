void showDeathScreen()
{
	Texture deathScreen;
	deathScreen.loadFromFile("images/deathScreen.png");
	Sprite deathScreenSprite;
	deathScreenSprite.setTexture(deathScreen);
	deathScreenSprite.setTextureRect(IntRect(0, 0, 600, 600));
	Clock clock;
	RenderWindow window1(VideoMode(600, 600), "DEATHSCREEN", Style::None);
	while (clock.getElapsedTime().asSeconds() <= 3)
	{
		window1.clear();
		deathScreenSprite.setPosition(0, 0);
		window1.draw(deathScreenSprite);
		window1.display();
	}
}
