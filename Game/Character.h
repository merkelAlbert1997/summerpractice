#include <SFML/Graphics.hpp>
#include <sstream>
using namespace sf;
tune coinUp("Coin.wav");
tune ringUp("Ring.wav");
tune murderEnemy("Kill.wav");
class Character : public entity
{
public:
	int health = 100;
	int scores = 0;
	float undeadTime = 0;
	bool onGround = false;
	bool undead = false;
	bool withRing = false;
	bool mission = false;
	bool winRat = false;
	bool isGamingWithRat = false;
	bool findGirl = false;
	int answers = 0;
	Character(String _f, float _x, float _y, float _w, float _h);
	void update(float _time);
	void collision(int dir);
	float getPlayerCoordinateX()
	{
		return x;
	}
	float getPlayerCoordinateY()
	{
		return y;
	}
	void setDirection()
	{
		if (Keyboard::isKeyPressed(Keyboard::Left))
		{
			dx = -DX;
			dir = -1;
			if (Keyboard::isKeyPressed(Keyboard::LShift))
			{
				dx = -3 * DX;
			}
		}
		if (Keyboard::isKeyPressed(Keyboard::Right))
		{
			dx = DX;
			dir = 1;
			if (Keyboard::isKeyPressed(Keyboard::LShift))
			{
				dx = 3 * DX;
			}
		}
		if (Keyboard::isKeyPressed(Keyboard::Up))
		{
			if (onGround)
			{
				onGround = false;
				dy = -0.65;
			}
		}

	}
	void setDialog(int lvl)
	{
		int i = 0;
		wstring *dialog = 0;
		wstring person;
		bool *achieve = 0;
		if (lvl == 1)
		{
			dialog = heroRichmanDialog;
			person = L"�����";
			achieve = &withRing;
		}
		if (lvl == 2)
		{
			dialog = heroKingDialog;
			person = L"������";
			achieve = &winRat;
		}
		if (lvl == 2 && mission == true && winRat==false)
		{
			dialog = heroRatDialog;
			person = L"�����";
			achieve = &winRat;
		}
		if (lvl == 3)
		{
			dialog = heroOldmanDialog;
			person = L"���";
			achieve = &findGirl;
		}
		if (lvl == 3 && mission==true && findGirl==false)
		{
			dialog = heroGirlDialog;
			person = L"���������";
			achieve = &findGirl;

		}
		setlocale(0, "Russian");
		Font font;
		font.loadFromFile("fonts/arial.ttf");
		Text text("", font, 15);
		text.setFont(font);
		text.setColor(Color::Black);
		RenderWindow window1(VideoMode(DIALOGWINDOWWIDTH, DIALOGWINDOWHEIGHT), "DIALOG", Style::None);
		while (window1.isOpen())
		{
			if (i > 5 && person == L"�����" && isGamingWithRat == false)
			{
				isGamingWithRat = true;
				window1.close();
				break;
			}
			window1.clear(Color(223, 158, 0));
			if (lvl == 3 && mission == true && findGirl == false)
			{
				text.setString(person + ":\n" + dialog[0] + '\n' + "1) " + dialog[1] + '\n');
			}
			else if (*achieve == true)
			{
				text.setString(person + ":\n" + dialog[9] + '\n' + "1) " + dialog[10] + '\n');
			}
			else
			{
				text.setString(person + ":\n" + dialog[i] + '\n' + L"�����:\n" + "1) " + dialog[i + 1] + '\n' + "2) " + dialog[i + 2] + '\n');
			}
			text.setPosition(20, 10);
			window1.draw(text);
			window1.display();
			Event event;
			while (window1.pollEvent(event))
			{
				if (event.type == event.KeyPressed)
				{
					if (Keyboard::isKeyPressed(Keyboard::Numpad1) || Keyboard::isKeyPressed(Keyboard::Num1))
					{
						if (lvl == 3 && mission == true && *achieve == false)
						{
							*achieve = true;
							window1.close();
							break;
						}
						if (*achieve == true)
						{
							keys++;
							window1.close();
							break;
						}
						if (i == 6)
						{
							cout << "w" << endl;
							mission = true;
							window1.close();
						}
						i += 3;
						break;
					}
					if (Keyboard::isKeyPressed(Keyboard::Numpad2) || Keyboard::isKeyPressed(Keyboard::Num2))
					{
						i += 3;
						window1.close();
						break;
					}
				}
			}
		}
	}
	void setHeroRatGame()
	{
		int i = 0;
		wstring *game = 0;
		wstring person;
		bool *achieve = 0;
		person = L"�����";
		achieve = &winRat;
		game = heroRatGame;
		setlocale(0, "Russian");
		Font font;
		font.loadFromFile("fonts/arial.ttf");
		Text text("", font, 15);
		text.setFont(font);
		text.setColor(Color::Black);
		RenderWindow window1(VideoMode(DIALOGWINDOWWIDTH, DIALOGWINDOWHEIGHT), "DIALOG", Style::None);
		while (window1.isOpen())
		{
			window1.clear(Color(223, 158, 0));
			if (i == 12)
			{
				text.setString(person + ":\n" + game[12] + '\n' + "1) " + game[14] + '\n');
			}
			else
			{
				text.setString(person + ":\n" + game[i] + '\n' + L"�����:\n" + "1) " + game[i + 1] + '\n' + "2) " + game[i + 2] + '\n' + "3) " + game[i + 3] + '\n');
			}
			text.setPosition(20, 10);
			window1.draw(text);
			window1.display();
			Event event;
			while (window1.pollEvent(event))
			{
				if (event.type == event.KeyPressed)
				{
					if (Keyboard::isKeyPressed(Keyboard::Num1) || Keyboard::isKeyPressed(Keyboard::Numpad1))
					{
						if (i == 12)
						{
							isGamingWithRat = false;
							winRat = true;
							window1.close();
						}
						if (i == 0 || i==8)
						{
							answers++;
							i += 4;
							continue;
						}
						if (i == 4)
						{
							Clock clock;
							while (clock.getElapsedTime().asSeconds() <= 3)
							{
								window1.clear(Color(223, 158, 0));
								text.setString(person + ":\n" + game[13] + '\n');				
								text.setPosition(20, 10);
								window1.draw(text);
								window1.display();
							}
							health = 0;
							window1.close();
							break;
						}
					}
					if (Keyboard::isKeyPressed(Keyboard::Num2) || Keyboard::isKeyPressed(Keyboard::Numpad2))
					{
						if (i == 4 || i==8)
						{
							answers++;
							i += 4;
							continue;
						}
						if (i == 0)
						{
							Clock clock;
							while (clock.getElapsedTime().asSeconds() <= 3)
							{
								window1.clear(Color(223, 158, 0));
								text.setString(person + ":\n" + game[13] + '\n');
								text.setPosition(20, 10);
								window1.draw(text);
								window1.display();
							}
							health = 0;
							window1.close();
							break;
						}
					}
					if (Keyboard::isKeyPressed(Keyboard::Num3) || Keyboard::isKeyPressed(Keyboard::Numpad3))
					{
						if (i == 8)
						{
							answers++;
							i += 4;
							continue;
						}
						if (i == 0 || i==4)
						{
							Clock clock;
							while (clock.getElapsedTime().asSeconds() <= 3)
							{
								window1.clear(Color(223, 158, 0));
								text.setString(person + ":\n" + game[i] + '\n' + L"�����:\n" + "1) " + game[i + 1] + '\n' + "2) " + game[i + 2] + '\n' + "3) " + game[i + 3] + '\n');
								text.setPosition(20, 10);
								window1.draw(text);
								window1.display();
							}
							health = 0;
							window1.close();
							break;
						}
					}
				}
			}
		}
	}
};
Character::Character(String _f, float _x, float _y, float _w, float _h)
{
	 scores = 0;
	 health = 100;
	 undeadTime = 0;
	 onGround = false;
	 undead = false;
	 withRing = false;
	 mission = false;
	 winRat = false;
	file = _f;
	x = _x;
	y = _y;
	w = _w;
	h = _h;
	Image hero_image;
	hero_image.loadFromFile("images/" + file);
	texture.loadFromImage(hero_image);
	sprite.setTexture(texture);
	sprite.setTextureRect(IntRect(0, 0, w, h));
	sprite.setPosition(x, y);
}

void Character::update(float _time)
{
	//std::cout << x << " " << y << std::endl;
	if (scores % 2000 == 0 && scores > 0)
	{
		lives++;
		scores -= 2000;
	}
	if (undead == true)
	{
		undeadTime = undeadClock.getElapsedTime().asSeconds();
	}
	else
	{
		undeadClock.restart();
	}
	if (undeadTime >= UNDEADTIME)
	{
		undead = false;
		sprite.setColor(sf::Color::White);
		undeadTime = 0;
	}
	x += dx*_time;
	collision(0);
	if (!onGround)
	{
		dy += 0.001*_time;
	}
	y += dy*_time;
	onGround = false;
	collision(1);
	currentFrame += 0.005 * _time;
	if (currentFrame > 2)
	{
		currentFrame -= 2;
	}
	if (dx > 0)
	{
		sprite.setTextureRect(IntRect(60 * static_cast<int>(currentFrame), 0, HEROWIDTH, HEROHEIGHT));
	}
	if (dx < 0)
	{
		sprite.setTextureRect(IntRect(60 * static_cast<int>(currentFrame), HEROHEIGHT, HEROWIDTH, HEROHEIGHT));
	}
	if (dx == 0)
	{
		if (dir == 1)
		{
			sprite.setTextureRect(IntRect(0, 0, HEROWIDTH, HEROHEIGHT));
		}
		if (dir == -1)
		{
			sprite.setTextureRect(IntRect(0, HEROHEIGHT, HEROWIDTH, HEROHEIGHT));
		}
	}
	if (dy < 0)
	{
		if (dir == 1)
		{
			sprite.setTextureRect(IntRect(0, HEROHEIGHT * 2, HEROWIDTH, HEROHEIGHT));
		}
		if (dir == -1)
		{
			sprite.setTextureRect(IntRect(60, HEROHEIGHT * 2, HEROWIDTH, HEROHEIGHT));
		}
	}
	getPlayerCoordinateForView(getPlayerCoordinateX(), getPlayerCoordinateY());
	dx = 0;
	sprite.setPosition(x, y);
}

void Character::collision(int dir)
{
	String *map = 0;
	if (level == 1) map = forestMap;
	if (level == 2) map = castleMap;
	if (level == 3) map = mountainMap;
	if (level == 4) map = caveMap;
	for (int i = y / TILEHEIGHT; i < (y + HEROHEIGHT) / TILEHEIGHT; i++)
	{
		for (int j = x / TILEWIDTH; j < (x + HEROWIDTH) / TILEWIDTH; j++)
		{
			if ((map[i][j] != '0' && map[i][j] != ' ' && map[i][j] != 'c'&& map[i][j] != 'r' && map[i][j] != 'f') || (i < 1) || (j < 1))
			{
				if (dx > 0 && dir == 0) x = j * TILEWIDTH - HEROWIDTH;//������������ ��� �������� �������
				if (dx < 0 && dir == 0) x = j * TILEWIDTH + TILEWIDTH;//������������ ��� �������� ������
				if (dy > 0 && dir == 1)//������������ ��� �������� ����
				{
					y = i * TILEHEIGHT - HEROHEIGHT;
					dy = 0;
					onGround = true;
				}
				if (dy < 0 && dir == 1) y = i * TILEHEIGHT + TILEHEIGHT;//������������ ��� �������� �����
			}
			if (map[i][j] == 'c')//������������ � ���������
			{
				coinUp.play();
				scores += 50;
				map[i][j] = ' ';
			}
			if (map[i][j] == 'r')//������������ � �������
			{
				ringUp.play();
				withRing = true;
				scores += 1000;
				map[i][j] = ' ';
			}
			if (map[i][j] == 's' && (i*TILEHEIGHT >= y + HEROHEIGHT) && undead==false)//������������ � ������
			{
				if (dy == 0)
				{
						health -= 20;
						undead = true;
						sprite.setColor(sf::Color::Red);
				}
			}
			if (i == HEIGHT_MAP)
			{
				health = 0;
			}
		}
	}
}
